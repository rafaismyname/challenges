class PatternMatcher
  attr_reader :pattern, :input

  def initialize(pattern, input)
    @pattern = pattern.to_s
    @input = input.to_s
  end

  def matches?
    # edge cases
    return true if @pattern == @input
    return false if !@input.size || !@pattern.size
    return false if @input.size < @pattern.size

    !!pattern_expression.match(@input)
  end

  def pattern_expression
    # reduce the pattern char array to generate the regex expression to do the match
    # every time a char appears for the first time in pattern string,
    # create (and append into expression) a new named capturing group using
    # the char as the capturing group name and mark captured group as created
    # following char aperances, append the backreference for its named capturing group into the expression
    # the word 'group' is prependded into the capturing group name so we can use numbers for our pattern
    initialized_groups = {}
    expression = @pattern.chars.reduce("") do |acc, char|
      is_new = initialized_groups[char].nil?
      initialized_groups[char] = true if is_new
      acc << (is_new ? "(?<group#{char}>.+)" : "\\k<group#{char}>")
    end

    Regexp.new("^#{expression}$") # return as a regex object
  end

  class << self
    def matches?(pattern, input)
      PatternMatcher.new(pattern, input).matches?
    end
  end
end
