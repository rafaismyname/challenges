require_relative "../pattern_matcher"

describe "PatternMatcher" do
  context "when input properly matches pattern" do
    it "should be truthy" do
      expect(PatternMatcher.matches?("abab", "redblueredblue")).to be_truthy
      expect(PatternMatcher.matches?("aaaa", "asdasdasdasd")).to be_truthy
      expect(PatternMatcher.matches?("abba", "xyyx")).to be_truthy
      expect(PatternMatcher.matches?("xyz", "abc")).to be_truthy
      expect(PatternMatcher.matches?("xyzzyx", "abccba")).to be_truthy
      expect(PatternMatcher.matches?("1234", "abcdefgh")).to be_truthy
    end
  end

  context "when input don't matches pattern" do
    it "should be falsey" do
      expect(PatternMatcher.matches?("aabb", "xyzabcxzyabc")).to be_falsey
      expect(PatternMatcher.matches?("abba", "xyzx")).to be_falsey
      expect(PatternMatcher.matches?("xyzz", "abcd")).to be_falsey
      expect(PatternMatcher.matches?("xyzzx", "abccba")).to be_falsey
      expect(PatternMatcher.matches?("12341", "abefgh")).to be_falsey
    end
  end
end
