# PatternMatcher
---

## About
Find if a string follows a pattern.  
It turns the `pattern` into regex capturing groups expression and checks if the `input` matches the generated expression  

## Usage examples
Pattern: "abab", input: "redblueredblue"  
`PatternMatcher.matches?("abab", "redblueredblue") # outputs: true`

Pattern: "aaaa", input: "asdasdasdasd"  
`PatternMatcher.matches?("aaaa", "asdasdasdasd") # outputs: true`

Pattern: "aabb", input: "xyzabcxzyabc"  
`PatternMatcher.matches?("aabb", "xyzabcxzyabc") # outputs: false`

## Testing
```
bundle install
bundle exec rspec
```
