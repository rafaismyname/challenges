import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Axios from 'axios';
import { Modal } from 'react-bootstrap';

import AddUser from './AddUser.jsx';
import UserRow from './UserRow.jsx';

// set token when request is made
// check them to cancel existing request if new one is made or component is unmounted
let CancelToken = Axios.CancelToken;
let cancel;

class TeamModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			type: this.props.type.charAt(0).toUpperCase() + this.props.type.slice(1),
			primaryUsers: [],
			inheritedUsers: [],
			eligibleUsers: [],
			currentUserPermission: '',
			hasError: false,
			owner: this.props.owner ? this.props.owner : {},
		}
	}

	componentDidMount = () => {
		this.getData();
	}

	componentWillReceiveProps = (nextProps) => {
		if (this.state.owner !== nextProps.owner) {
			this.setState({
				owner: nextProps.owner,
			})
		}
}

	componentWillUnmount = () => {
	  // check for cancel token to see if a request is running
	  // cleanup and cancel existing requests
	  if (cancel !== undefined) {
	    cancel();
	  }
	}

	getUserName = (user) => {
		if (user.short_name) {
			return user.short_name.charAt(0).toUpperCase() + user.short_name.slice(1);
		} else if (user.first_name && user.last_name) {
			return (
				user.first_name.charAt(0).toUpperCase() + user.first_name.slice(1)
				+ " "
				+ user.last_name.charAt(0).toUpperCase() + user.last_name.slice(1)
			)
		} else {
			return user.email;
		}
	}

	getTeamList = () => {
		if (this.state.primaryUsers.length > 0) {
			return(
				this.state.primaryUsers.map((user) => {
					if (!this.state.owner || user.id !== this.state.owner.id) {
						return(
							<UserRow
								key={`modal-${this.props.objectId}-member-${user.id}`}
								user={user}
								objectType={this.props.type}
								objectId={this.props.objectId}
								updateCallback={this.updateData}
								updateUsersCallback={this.updateUsers}
								matterSlug={this.props.matterSlug}
								ownerId={this.state.owner ? this.state.owner.id : null}
								currentUserRole={this.props.currentUserRole}
								currentUserPermission={this.state.currentUserPermission}
								eligibleUsers={this.state.eligibleUsers}
							/>
						);
					}
				})
			);
		} else if (this.state.hasError) {
			return(<p className="lead text-center">We were unable to retrieve the members of this {this.props.type}.</p>);
		}
	}

	getAccessList = (users) => {
		let inheritedUsers = '';

		users.map((user, index) => {
			switch (true) {
				case index === 0:
					inheritedUsers = this.getUserName(user);
					break;
				case index === users.length - 1:
					inheritedUsers = `${inheritedUsers} and ${this.getUserName(user)}`;
					break;
				default:
					inheritedUsers = `${inheritedUsers}, ${this.getUserName(user)}`;
					break;
			}
		});

		inheritedUsers = <span><span className="bold-text">{inheritedUsers}</span> {users.length > 1 ? 'have' : 'has'}</span>

		return inheritedUsers;
	}

	getData = (owner, successMessage) => {
		Axios({
		  method: 'get',
		  url: `/access/share_widget.json?object_id=${this.props.objectId}&object_type=${this.props.type.toLowerCase()}`,
		  headers: {
		    Accept: 'application/json',
				'X-Requested-With': 'XMLHttpRequest',
		  },
		  cancelToken: new CancelToken(function executor(c) {
		    cancel = c;
		  })
		}).then((response) => {
			let primaryUsers = response.data.users.primary.edit.concat(response.data.users.primary.view);

			this.setState({
				currentUserPermission: response.data.current_user_permission,
				primaryUsers: primaryUsers,
				inheritedUsers: response.data.users.inherited,
				eligibleUsers: response.data.users.all_users,
				hasError: false,
				matterName: response.data.users.inherited.matter.name ? response.data.users.inherited.matter.name : 'the matter',
			});

			if (owner) {
				// Update owner in parent component (widget)
				this.props.updateCallback(owner, successMessage);
			}

			cancel = undefined;
		}).catch((err) => {
		  console.log(err);

		  this.setState({
		  	hasError: true,
		  });
		  cancel = undefined;
		});
	}

	updateData = (owner, successMessage) => {
		// Update team list in this modal
		this.getData(owner, successMessage);
	}

	updateUsers = () => {
		this.getData();
		this.props.updateUsersCallback ? this.props.updateUsersCallback() : null;
	}

	render () {
		return(
			<div>
				<Modal.Header closeButton>
					<Modal.Title>Access to <span className="bold-text">{this.props.objectName}</span></Modal.Title>
				</Modal.Header>
				<Modal.Body>

					<AddUser
						type={this.state.type.toLowerCase()}
						objectName={this.props.objectName}
						currentUserPermission={this.state.currentUserPermission}
						shareMessage={this.state.shareMessage}
						objectId={this.props.objectId}
						updateCallback={this.updateUsers}
						matterSlug={this.props.matterSlug}

					/>

					<hr />

					<div className="team-list">
						<div>
							{this.state.owner
								&& this.state.owner !== null
								&& Object.keys(this.state.owner).length !== 0 ?
								<UserRow
									key={`modal-${this.props.objectId}-member-${this.state.owner.id}`}
									user={this.state.owner}
									objectType={this.props.type}
									objectId={this.props.objectId}
									updateCallback={this.updateData}
									updateUsersCallback={this.updateUsers}
									matterSlug={this.props.matterSlug}
									currentUserRole={this.props.currentUserRole}
									currentUserPermission={this.state.currentUserPermission}
									eligibleUsers={this.state.eligibleUsers}
									isOwner
								/>
							: null }
							{this.getTeamList()}

							{this.state.inheritedUsers.matter
								&& this.state.inheritedUsers.matter !== null
								&& Object.keys(this.state.inheritedUsers.matter).length !== 0
								&& this.state.inheritedUsers.matter.edit.length > 0 ?
								<h4>{this.getAccessList(this.state.inheritedUsers.matter.edit)} access to edit this {this.props.type.toLowerCase()} through <span className="bold-text">{this.state.matterName}</span>.</h4>
							: null }

							{this.state.inheritedUsers.matter
								&& this.state.inheritedUsers.matter !== null
								&& Object.keys(this.state.inheritedUsers.matter).length !== 0
								&& this.state.inheritedUsers.matter.view.length > 0 ?
								<h4>{this.getAccessList(this.state.inheritedUsers.matter.view)} access to view this {this.props.type.toLowerCase()} through <span className="bold-text">{this.state.matterName}</span>.</h4>
							: null }

							{this.state.inheritedUsers.task
								&& this.state.inheritedUsers.task !== null
								&& Object.keys(this.state.inheritedUsers.task).length !== 0
								&& this.state.inheritedUsers.task.edit.length > 0 ?
								<h4>{this.getAccessList(this.state.inheritedUsers.task.edit)} access to edit this {this.props.type.toLowerCase()} through the <span className="bold-text">{this.state.matterName}</span> tasks.</h4>
							: null }

							{this.state.inheritedUsers.task
								&& this.state.inheritedUsers.task !== null
								&& Object.keys(this.state.inheritedUsers.task).length !== 0
								&& this.state.inheritedUsers.task.view.length > 0 ?
								<h4>{this.getAccessList(this.state.inheritedUsers.task.view)} access to view this {this.props.type.toLowerCase()} through the <span className="bold-text">{this.state.matterName}</span> tasks.</h4>
							: null }
						</div>
					</div>
				</Modal.Body>
			</div>
		)
	}
}

TeamModal.propTypes = {
	objectId: PropTypes.number.isRequired,
	updateCallback: PropTypes.func,
	updateUsersCallback: PropTypes.func,
	objectName: PropTypes.string,
	owner: PropTypes.object,
	ownerId: PropTypes.number,
	matterSlug: PropTypes.string,
	currentUserRole: PropTypes.string,
	type: PropTypes.string,
}

TeamModal.defaultProps = {
	updateUsersCallback: null,
	owner: {},
	ownerId: null,
	matterSlug: '',
	currentUserRole: 'user',
	type: 'Task',
}

export default TeamModal;
