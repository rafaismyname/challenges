import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, FormControl, InputGroup, Button } from 'react-bootstrap';

class DynamicInputForm extends Component {
  constructor() {
    super();
    this.state = {
      inputContents: [],
    };
  }

  handleinputContentChange = (index) => (e) => {
    const newinputContents = this.state.inputContents.map((inputContent, inputContentIndex) => {
    	if (index !== inputContentIndex) {
    		return inputContent;
    	} else {
    	  return e.target.value;
    	}
    });

    this.setState({
    	inputContents: newinputContents,
    });

    this.props.updateCallback(newinputContents);
  }

  handleAddinputContent = () => {
    this.setState({
      inputContents: this.state.inputContents.concat([''])
    });
  }

  handleRemoveinputContent = (index) => () => {
  	const newinputContents = this.state.inputContents.filter((inputContent, inputContentIndex) => index !== inputContentIndex);

    this.setState({
      inputContents: newinputContents,
    });

    this.props.updateCallback(newinputContents);
  }

  render() {
    return (
      <div>
        {this.state.inputContents.map((inputContent, index) => (
        	<FormGroup key={`custom-req-${index}`}>
	        	<InputGroup>
		          <FormControl
		            type="text"
		            value={inputContent}
		            placeholder={this.props.placeholderText}
		            onChange={this.handleinputContentChange(index)}
		          />
		          <InputGroup.Button>
			          <Button
			          	type="button"
			          	onClick={this.handleRemoveinputContent(index)}
			          	className="btn btn_icon btn-close"
		          	>
		          		<i className="fa fa-lg fa-trash" />
		          	</Button>
	          	</InputGroup.Button>
          	</InputGroup>
          </FormGroup>
	      ))}

        <Button
        	type="button"
        	onClick={this.handleAddinputContent}
        	bsStyle="link"
        >
        	{this.props.buttonText}
        </Button>
      </div>
    )
  }
}

DynamicInputForm.propTypes = {
	updateCallback: PropTypes.func.isRequired,
  placeholderText: PropTypes.string,
  buttonText: PropTypes.string,
}

DynamicInputForm.defaultProps = {
  placeholderText: "Enter information",
  buttonText: "+ Add item",
}

export default DynamicInputForm;
