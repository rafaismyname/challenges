import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

import './Elements.css';

// Parent element must have position:relative for correct badge placement
const CountCircle = (props) => (
	<div className="circle-badge text-center">
		<p className="small">{props.content}</p>
	</div>
);

CountCircle.propTypes = {
	content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

CountCircle.defaultProps = {
  content: null,
};

export default CountCircle;